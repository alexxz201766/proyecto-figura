//
//  CEquilatero.hpp
//  poo211
//
//  Created by Ahgala on 10/11/20.
//

#ifndef CEquilatero_hpp
#define CEquilatero_hpp

#include "CTriangulo.hpp"

class CEquilatero: public CTriangulo{
public:
  CEquilatero();
  CEquilatero(float);
  CEquilatero(float,float);
  void setLado(int,float);
};



#endif

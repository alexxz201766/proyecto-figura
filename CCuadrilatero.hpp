//
//  CCuadrilatero.hpp
//  Proyecto Figura
//
//  Created by ALEJANDRO TOLENTINO on 17/11/20.
//

#ifndef CCuadrilatero_hpp
#define CCuadrilatero_hpp
#include "CFigura.hpp"

#include <stdio.h>

class CCuadrilatero : public CFigura {
  private:
    float cuadri1,cuadri2,cuadri3,cuadri4;
    float cuadrialtura;
  public:
    CCuadrilatero();
    CCuadrilatero(float,float,float,float);
    CCuadrilatero(float,float,float,float,float);
 
    float Cperimetro(void);
    float Carea(void);
        
    void setNumLado(int,float);
    float getNumlado(int);
    
    void setLado(int,float);
    float getLado(int);
      
};


#endif /* CCuadrilatero_hpp */

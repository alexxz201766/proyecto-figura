//
//  CCuadrilatero.cpp
//  Proyecto Figura
//
//  Created by ALEJANDRO TOLENTINO on 17/11/20.
//

#include "CCuadrilatero.hpp"

CCuadrilatero::CCuadrilatero():CFigura(4) {
    cuadri1=0;
    cuadri2=0;
    cuadri3=0;
    cuadri4=0;
    cuadrialtura=0;
}

CCuadrilatero::CCuadrilatero(float lado1,float lado2,float lado3,float lado4):CFigura(4){
    cuadri1=lado1;
    cuadri2=lado2;
    cuadri3=lado3;
    cuadri4=lado4;
    cuadrialtura=lado1;
}

CCuadrilatero::CCuadrilatero(float lado1,float lado2,float lado3,float lado4,float altura):CFigura(4){
    cuadri1=lado1;
    cuadri2=lado2;
    cuadri3=lado3;
    cuadri4=lado4;
    cuadrialtura=altura;
}
float CCuadrilatero::Cperimetro(){
    return cuadri1+cuadri2+cuadri3+cuadri4;
}
float CCuadrilatero::Carea(){
    return cuadri1*cuadri2;
}
    
    void CCuadrilatero::setLado(int lado,float valor){
      switch (lado) {
        case 1:
          cuadri1=valor;
          break;
        case 2:
          cuadri2=valor;
          break;
        case 3:
          cuadri3=valor;
          break;
        case 4:
          cuadri4=valor;
          case 5:
              cuadrialtura=valor;
          break;
        default:
          break;
      }
    }

float CCuadrilatero::getLado(int lado){
  switch (lado) {
    case 1:
      return cuadri1;
    case 2:
      return cuadri2;
    case 3:
      return cuadri3;
    case 4:
      return cuadri4;
      case 5:
     return cuadrialtura;
    default:
      return 0;
  }
}


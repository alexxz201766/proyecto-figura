//
//  CRectangulo.hpp
//  Proyecto Figura
//
//  Created by ALEJANDRO TOLENTINO on 18/11/20.
//

#ifndef CRectangulo_hpp
#define CRectangulo_hpp
#include "CCuadrilatero.hpp"
#include <stdio.h>

class CRectangulo: public CCuadrilatero{
public:
    CRectangulo();
    CRectangulo(float,float,float,float);
    CRectangulo(float,float,float,float,float);
    
    void setLado(int,float);
    float getLado(int);
    
};

#endif /* CRectangulo_hpp */

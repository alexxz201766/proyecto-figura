
//
//  CIsoceles.hpp
//  poo211
//
//  Created by Ahgala on 10/11/20.
//

#ifndef CIsoceles_hpp
#define CIsoceles_hpp

#include "CTriangulo.hpp"

class CIsoceles: public CTriangulo{
public:
  CIsoceles();
  CIsoceles(float,float);
  CIsoceles(float,float,float);
  void setLado(int,float);
};

#endif /* CIsoceles_hpp */


//
//  main.cpp
//  poo211
//
//

#include <iostream>
#include "CEquilatero.hpp"
#include "CIsoceles.hpp"

using namespace std;

int main(void) {

  CIsoceles f(5,8,10);

  f.setLado(1, 3);
  f.setLado(2,9);
  f.setLado(3,13);
  f.setLado(4,10);

  cout << "El número de lados del triángulo F es: " << f.getNumLados() << "\n";
  cout << "El área del triángulo F es: " << f.area() << "\n";
  cout << "El perímetro del triángulo F es: " << f.perimetro()<< "\n";

  cout << "El lado 1 mide: "<< f.getLado(1)<< ", el lado 2 mide: "<< f.getLado(2)<< ", el lado 3 mide: "<< f.getLado(3) << " y la altura mide: " << f.getLado(4) << " del triángulo F.\n";

    
    
  return 0;
}
    

